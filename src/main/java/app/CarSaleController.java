package app;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.file.Files;
import java.nio.file.Paths;

@RestController
public class CarSaleController {

    @RequestMapping("/")
    String render() {
        return "Welcome to The Purple Car, an online car sale company";
    }

    @RequestMapping("/add")
    public String render(@RequestBody Car car) throws Exception {
        try {
            return "Added car: " + car.toString();
        } catch (Exception e) {
            return "Sorry! Something went wrong.";
        }
    }

    @RequestMapping("/loadFromFile")
    public String loadFromFile() throws Exception {
        Key key = new Key();
        char[] secretKey = key.retrieveKey();
        String fileContents = new String(Files.readAllBytes(Paths.get("/Users/motero/code/applications/purple-car/testFile")));
        return key.decrypt(secretKey, fileContents);
    }
}
