package app;

public class CarId {
    private Integer carId;

    public CarId() {
    }

    public CarId(Integer carId) {
        checkNotEmpty(carId);
        checkNotNegative(carId);
    }

    private void checkNotNegative(Integer carId) {
        if (carId < 0) {
            throw new IllegalArgumentException("carId should not be negative");
        }
    }

    private void checkNotEmpty(Integer carId) {
        if (carId == null) {
            throw new IllegalArgumentException("carId should not be empty");
        }
    }

    public Integer getCarId() {
        return carId;
    }

    @Override
    public String toString() {
        return "CarId{" +
                "carId=" + carId +
                '}';
    }
}
