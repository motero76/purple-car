package app;

import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Model {
    private String model;

    public Model() {
    }

    public Model(String model) {
        checkNotEmpty(model);
        checkLength(model);
        checkAlphanumericOrSpaces(model);
    }

    private void checkLength(String model) {
        if (model.length() < 2 || model.length() > 19) {
            throw new IllegalArgumentException("model must be between 2 and 19 characters inclusive");
        }
    }

    private void checkNotEmpty(String model) {
        if (StringUtils.isEmpty(model)) {
            throw new IllegalArgumentException("model must not be empty");
        }
    }

    public String getModel() {
        return model;
    }


    private void checkAlphanumericOrSpaces(String model) {
        String regex = "^[a-zA-Z0-9\\s]+$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(model);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("model must be alphanumeric");
        }
    }


    @Override
    public String toString() {
        return "Model{" +
                "model='" + model + '\'' +
                '}';
    }
}
