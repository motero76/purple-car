package app;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;

public class Key {
    private char[] key;
    private boolean consumed = false;

    public Key() {
        this.key = System.getProperty("secretKey").toCharArray();
    }

    public char[] retrieveKey() {
        if (consumed) {
            throw new RuntimeException("Key has already been consumed");
        }
        char[] keyToReturn = key.clone();
        Arrays.fill(key, '0');
        consumed = true;
        return keyToReturn;
    }

    public String decrypt(char[] secretKey, String encrypted) throws Exception {
        String salt = "ssshhhhhhhhhhh!!!!";
        byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        IvParameterSpec ivspec = new IvParameterSpec(iv);

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(secretKey, salt.getBytes(), 65536, 256);
        SecretKey tmp = factory.generateSecret(spec);
        SecretKeySpec decryptKey = new SecretKeySpec(tmp.getEncoded(), "AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, decryptKey, ivspec);
        return new String(cipher.doFinal(Base64.getDecoder().decode(encrypted)));
    }

    /**
     * Util method to create encrypted file contents.
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
            byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            String salt = "ssshhhhhhhhhhh!!!!";
            IvParameterSpec ivspec = new IvParameterSpec(iv);

            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec("boooooooooom!!!!".toCharArray(), salt.getBytes(), 65536, 256);
            SecretKey tmp = factory.generateSecret(spec);
            SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
            String fileContents = "File contents have been decrypted!";
            System.out.println(Base64.getEncoder().encodeToString(cipher.doFinal(fileContents.getBytes("UTF-8"))));
        } catch (Exception e) {
            System.out.println("Something went wrong!");
        }
    }
}
