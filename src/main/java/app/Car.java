package app;

public class Car {

    private CarId carId;
    private Vin vin;
    private Model model;
    //private String plateNo;
    //private File photo;
    
    public Car() {
        super();
    }

    public Car(CarId carId, Vin vin, Model model) {
        this.carId = carId;
        this.vin = vin;
        this.model = model;
    }

    public CarId getcarID() {
        return this.carId;
    }

    public Vin getvin() {
        return this.vin;
    }
    
    public Model getmodel() {
        return this.model;
    }

    @Override
    public String toString() {
        return this.carId.toString() + " "  + this.vin.toString() + " " + this.model.toString();
    }

}
