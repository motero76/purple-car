package app;

import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Vin {
    private String vin;

    public Vin() {
    }

    Vin(String vin) {
        checkNotEmpty(vin);
        checkAlphanumeric(vin);
        checkLength(vin);
    }

    private void checkLength(String vin) {
        if (vin.length() != 17) {
            throw new IllegalArgumentException("vin should be 17 characters");
        }
    }

    private void checkAlphanumeric(String vin) {
        String regex = "^[a-zA-Z0-9]+$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(vin);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("vin must be alphanumeric");
        }

    }

    private void checkNotEmpty(String vin) {
        if (StringUtils.isEmpty(vin)) {
            throw new IllegalArgumentException("vin should not be empty");
        }
    }

    public String getVin() {
        return vin;
    }

    @Override
    public String toString() {
        return "Vin{" +
                "vin='" + vin + '\'' +
                '}';
    }
}
