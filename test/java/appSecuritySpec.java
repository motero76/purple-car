package app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@DisplayName("Usability unit tests")
@WebMvcTest
@Tag("security")
public class appSecuritySpec {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_return_error_message_for_invalid_carid() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            CarId carId = new CarId(-5);
        });
    }

    @Test
    public void should_return_error_message_for_empty_model() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Model model = new Model(null);
        });

    }

    @Test
    public void should_return_error_message_for_empty_vin() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Vin vin = new Vin("");
        });
    }

    @Test
    public void should_return_error_message_for_numeric_vin() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Vin vin = new Vin("123");
        });
    }

    @Test
    public void should_return_error_message_for_non_alphanumeric_model() throws Exception {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Model model = new Model("123--");
        });
    }
}
