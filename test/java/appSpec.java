package app;

import org.junit.jupiter.api.DisplayName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@DisplayName("Usability unit tests")
@WebMvcTest
public class appSpec {

    @Autowired
    private MockMvc mockMvc;

//    @Test
//    public void should_outputCarID_whenValidRequest()
//        throws JsonParseException, IOException, Exception {
//        Car car = new Car(new CarId(1), new Vin("ABC12345678901234"), new Model("foo"));
//        ObjectMapper mapper = new ObjectMapper();
//        this.mockMvc.perform(post("/add")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(mapper.writeValueAsString(car))
//                .accept(MediaType.APPLICATION_JSON))
//            .andExpect(status().isOk())
//            .andExpect(content().string(containsString("1")));
//    }
}
